#include "qandroidplatformwindowmanager.h"

#include "qandroidplatformwindow.h"
#include "qandroidplatformbackingstore.h"

#include <private/qhighdpiscaling_p.h>
#include <QPainter>
#include <qbackingstore.h>

// In HighDPI
static const int paddingTop = 25;
static const int paddingLBR = 6; // left, bottom and right

QAndroidPlatformWindowManager::QAndroidPlatformWindowManager(QAndroidPlatformWindow *window)
    : QWindow(window->window()->parent())
    , m_realWindow(window)
    , m_backingStore(new QAndroidPlatformBackingStore(this))
{
    setObjectName("QAndroidPlatformWindowManager");
    // popup because they are special, frameless because recursion
    setFlags(Qt::Popup | Qt::FramelessWindowHint);
    setMinimumSize(window->window()->minimumSize());
    setMaximumSize(window->window()->maximumSize());
}

QAndroidPlatformWindowManager::~QAndroidPlatformWindowManager()
{
    delete m_backingStore;
}

void QAndroidPlatformWindowManager::updateGeometry(const QRect &rect)
{
    if (m_oldChildGeometry == rect) {
        return;
    }
    m_oldChildGeometry = rect;
    QRect wmGeometry = QHighDpi::fromNativePixels(rect, m_realWindow->window());
    wmGeometry.translate(-paddingLBR, -paddingTop);
    wmGeometry.setSize({ 2 * paddingLBR + wmGeometry.width(),
                            paddingLBR + wmGeometry.height() + paddingTop });

    setGeometry(wmGeometry);
    resize(QHighDpi::toNativePixels(geometry().size(), this));
}

void QAndroidPlatformWindowManager::raiseRealWindow()
{
    m_realWindow->raise();
}

void QAndroidPlatformWindowManager::lowerRealWindow()
{
    m_realWindow->lower();
}

bool QAndroidPlatformWindowManager::contains(QPoint point, bool inNativeUnits)
{
    if (inNativeUnits) {
        point = QHighDpi::fromNativePixels(point, this);
    }
    const QRegion wmGeom = geometry();
    const QRegion childGeom = m_realWindow->window()->geometry();

    return wmGeom.subtracted(childGeom).contains(point);
}

void QAndroidPlatformWindowManager::mousePressEvent(QMouseEvent *event)
{
    m_startingPoint = event->globalPos();
    m_realWindowOffset = m_startingPoint - m_realWindow->window()->geometry().topLeft();
    m_startingGeometry = m_realWindow->window()->geometry();

    calculatePressArea(event->globalPos());

    m_mousePressed = true;
    event->accept();
}

void QAndroidPlatformWindowManager::calculatePressArea(const QPoint &pos)
{
    QRect wmGeom = geometry();
    QRect childGeom = m_realWindow->window()->geometry();

    // NOTE: we add + 1 because of QRect's quirks
    if (QRect(wmGeom.topLeft(), QSize(wmGeom.width(), paddingTop)).contains(pos)) {
        m_grabbedAction = MOVE;
    } else if (QRect(wmGeom.topLeft(), QSize(paddingLBR, wmGeom.height()))
                   .contains(pos)) {
        m_grabbedAction = HORIZONTAL_RESIZE_LEFT;
    } else if (QRect({childGeom.right() + 1, childGeom.top()},
                     QSize(paddingLBR, wmGeom.height()))
                   .contains(pos)) {
        m_grabbedAction = HORIZONTAL_RESIZE_RIGHT;
    } else if (QRect({childGeom.left(), childGeom.bottom() + 1}, childGeom.size())
                   .contains(pos)) {
        m_grabbedAction = VERTICAL_RESIZE;
    }

    int cornerPadding = 10; // this makes corners a bit easier to grab

    if (QRect(childGeom.translated(-cornerPadding, -cornerPadding).bottomRight(),
              QPoint(wmGeom.right() + 1, wmGeom.bottom() + 1))
            .contains(pos)) {
        m_grabbedAction = CORNER_RESIZE_RIGHT;
    }

    QPoint startOffset = QPoint(wmGeom.left(), childGeom.bottom() - cornerPadding);
    if (QRect(startOffset, QPoint(childGeom.left() + cornerPadding, wmGeom.bottom()))
            .contains(pos)) {
        m_grabbedAction = CORNER_RESIZE_LEFT;
    }
}

void QAndroidPlatformWindowManager::mouseMoveEvent(QMouseEvent *event)
{
    if (m_mousePressed) {
        move(event->globalPos());
        event->accept();
    }
}

void QAndroidPlatformWindowManager::mouseReleaseEvent(QMouseEvent *event)
{
    m_mousePressed = false;
    m_grabbedAction = NONE;

    event->accept();
}

void QAndroidPlatformWindowManager::resizeEvent(QResizeEvent *event)
{
    resize(event->size());
}

void QAndroidPlatformWindowManager::showEvent(QShowEvent *event)
{
    if (m_realWindow->geometry().topLeft().y() < paddingTop) {
        QRect geom = m_realWindow->geometry();
        geom.translate(0, paddingTop);
        m_realWindow->setGeometry(geom);
    }

    updateGeometry(m_realWindow->geometry());

    setWindowState(Qt::WindowNoState);
    // NOTE: platformWindow won't be created yet, so set flags for it.
    // popup because they are special, frameless because recursion
    setFlags(Qt::Popup | Qt::FramelessWindowHint);

    event->accept();
}

void QAndroidPlatformWindowManager::repaint()
{
    QRect childGeometry = handle()->geometry();
    QAndroidPlatformWindow *platformWindow = static_cast<QAndroidPlatformWindow *>(handle());
    platformWindow->setBackingStore(m_backingStore);

    QPainter painter(m_backingStore->paintDevice());
    painter.fillRect(0, 0, childGeometry.width(), childGeometry.height(), Qt::white);
    QPen pen(Qt::black);
    pen.setWidth(6);
    painter.setPen(pen);
    painter.drawRect(0, 0, childGeometry.width(), childGeometry.height());

    m_backingStore->endPaint();

    // m_backingStore->toImage().save(QString("/data/data/org.krita/files/%1.png").arg(this->objectName()));
}

void QAndroidPlatformWindowManager::move(const QPoint &pos)
{
    switch (m_grabbedAction) {
    case MOVE:
        m_realWindow->window()->setPosition(QPoint(pos.x(), std::max(10, pos.y()))
                                            - m_realWindowOffset);
        break;
    case HORIZONTAL_RESIZE_LEFT:
        resizeChildWindow(QRect(pos.x(), m_startingGeometry.y(),
                                m_startingGeometry.right() - pos.x(),
                                m_startingGeometry.height()));
        break;
    case HORIZONTAL_RESIZE_RIGHT:
        resizeChildWindow(QRect(m_startingGeometry.left(), m_startingGeometry.top(),
                                (pos.x() - m_startingGeometry.left()),
                                m_startingGeometry.height()));
        break;
    case VERTICAL_RESIZE:
        resizeChildWindow(QRect(m_startingGeometry.left(), m_startingGeometry.top(),
                                m_startingGeometry.width(),
                                (pos.y() - m_startingGeometry.top())));
        break;
    case CORNER_RESIZE_LEFT:
        resizeChildWindow(QRect(pos.x(), m_startingGeometry.y(),
                                m_startingGeometry.right() - pos.x(),
                                pos.y() - m_startingGeometry.top()));
        break;
    case CORNER_RESIZE_RIGHT:
        resizeChildWindow(QRect(m_startingGeometry.left(), m_startingGeometry.top(),
                                (pos.x() - m_startingGeometry.left()),
                                (pos.y() - m_startingGeometry.top())));
        break;
    default:
        qWarning() << "WindowManager: Bad Action" << m_grabbedAction;
        break;
    }
}

void QAndroidPlatformWindowManager::resize(const QSize &size)
{
    if (m_oldSize == size) {
        return;
    }
    m_oldSize = size;
    m_backingStore->resize(QHighDpi::toNativePixels(size, this), QRegion());
    repaint();
}

void QAndroidPlatformWindowManager::resizeChildWindow(const QRect &geom)
{
    QRect adjustedGeom = geom;
    if (adjustedGeom.width() < minimumWidth()) {
        adjustedGeom.setLeft(m_realWindow->window()->geometry().left());
        adjustedGeom.setWidth(minimumWidth());
    }
    if (adjustedGeom.height() < minimumHeight()) {
        adjustedGeom.setTop(m_realWindow->window()->geometry().top());
        adjustedGeom.setHeight(minimumHeight());
    }

    adjustedGeom.setWidth(adjustedGeom.width() > maximumWidth() ? maximumWidth()
                                                                : adjustedGeom.width());

    adjustedGeom.setHeight(adjustedGeom.height() > maximumHeight()
                               ? maximumHeight()
                               : adjustedGeom.height());

    // this will trigger window manager's updateGeometry as well
    m_realWindow->window()->setGeometry(adjustedGeom);
}
