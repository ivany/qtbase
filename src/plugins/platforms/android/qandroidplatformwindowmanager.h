#ifndef __QANDROIDPLATFORMWINDOWMANAGER_H_
#define __QANDROIDPLATFORMWINDOWMANAGER_H_

#include <qpa/qplatformwindow.h>

class QAndroidPlatformWindow;
class QAndroidPlatformBackingStore;

class QAndroidPlatformWindowManager: public QWindow {
private:
    enum WM_ACTION {
        MOVE = 0,
        HORIZONTAL_RESIZE_LEFT,
        HORIZONTAL_RESIZE_RIGHT,
        VERTICAL_RESIZE,
        CORNER_RESIZE_LEFT,
        CORNER_RESIZE_RIGHT,
        NONE
    };
public:
    QAndroidPlatformWindowManager(QAndroidPlatformWindow *window = nullptr);
    ~QAndroidPlatformWindowManager();

    /*
     * Updates the geometry of the window manager as per its child
     * @param geometry of window to be managed, rect should be in native pixels
     */
    void updateGeometry(const QRect &rect);
    void raiseRealWindow();
    void lowerRealWindow();
    QAndroidPlatformWindow *realWindow() { return m_realWindow; }

    /**
     * return true of the window type needs a window manager.
     */
    static bool needsWindowManager(QWindow *window)
    {
        return ((window->type() == Qt::Dialog || window->type() == Qt::Tool)
                && (window->flags() & Qt::FramelessWindowHint) == 0);
    }

    /**
     * Returns true if point is in the window manager bounds, but not in the bounds of its
     * child/real window. If the point is in native units, inNativeUnits should be set to true for
     * valid calulcations.
     */
    bool contains(QPoint point, bool inNativeUnits = false);

protected:
    void resizeEvent(QResizeEvent *event) override;
    void showEvent(QShowEvent *event) override;

    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

private:
    void repaint();
    void calculatePressArea(const QPoint &pos);
    void move(const QPoint &pos);
    void resize(const QSize &size);
    void resizeChildWindow(const QRect &geom);

private:
    QAndroidPlatformWindow *m_realWindow;
    QAndroidPlatformBackingStore *m_backingStore;

    QPoint m_startingPoint;
    QRect m_startingGeometry;
    QPoint m_realWindowOffset;
    QRect m_oldChildGeometry;
    QSize m_oldSize;
    bool m_mousePressed {false}; // because synthesized events
    WM_ACTION m_grabbedAction { NONE };
};


#endif // __QANDROIDPLATFORMWINDOWMANAGER_H_
